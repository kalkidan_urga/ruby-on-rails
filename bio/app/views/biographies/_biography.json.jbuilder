json.extract! biography, :id, :name, :biography, :created_at, :updated_at
json.url biography_url(biography, format: :json)
