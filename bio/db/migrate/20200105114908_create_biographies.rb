class CreateBiographies < ActiveRecord::Migration[6.0]
  def change
    create_table :biographies do |t|
      t.string :name
      t.string :biography

      t.timestamps
    end
  end
end
